using namespace std;

//////////////////////////////////////////////////

class chainlink {
public:
    int index;
    int level;
    int turns;
};  
  
class Sector {
public:
    int number;
    string owner;
    string capital;
    int FF;      // fleets
       
    int Rbonus[4];  // bonus 0:resources, 1:credits, 2:science, 3:experience
    int totbonus();
    int RR;         // resources when rebel sector
    int Glevel[4];  // level 0:raffinery, 1:industry, 2:technosphere, 3:military
    vector<string> megastructures;
    int gain(int resourceindex);
    vector<chainlink> Gchain;   /// generator construction chain
    vector<chainlink> Tchain;   /// technosphere chain
    vector<chainlink> Fchain;   /// fleet construction chain
    
    bool iscentral();
    bool isring();
    bool isbridge();
    bool isperiphery();
       
    int victory_points();
    void print();
};  

class Mission {
public:  
    int Asector;   // provenance
    int Bsector;   // destination
    int nfleets;
    int nturns;
    int type;   // 0:reinforcements, 1:colonisation, 2:conquest, 3:raid
    int outgoing;
};  

class Player {
public:
    string name;   // faction name
    
    vector<Sector*> sectors;  
    
    int RR[4];  // 0:resources, 1:credits, 2:science, 3:experience
    
    int Tlevel[nTech];   // 0:defense, 1:attaque, 2:admin étendue, 3:architecture, 4:conquête
    
    vector<Mission> missions;
    
    vector<int> hyperdrive(vector<Sector>,int,int);
    
    int victory_points();
    void print();

private:
    vector<int> adjsect[36];
};  

int Sector::totbonus() {
   return Rbonus[0]+Rbonus[1]+Rbonus[2]+Rbonus[3];
}  

int Sector::gain(int resourceindex) {
  int gain=Rbonus[resourceindex]*(Glevel[resourceindex]+1);
  /// faction bonuses
  if(owner=="Nerve" && resourceindex==0) gain+=0.121*gain;
  if(owner=="Négore" && resourceindex==1) gain+=0.121*gain;
  if(owner=="Aphéra" && resourceindex==2) gain+=0.121*gain;
  if(owner=="Kovahk" && resourceindex==3) gain+=0.121*gain;
  return gain;
}

int battle(int nfleets0,int level0,int nfleets1,int level1) {
  while(nfleets0>0 && nfleets1>0) {
    int strike0=rand()%(basestrike+level0);
    int strike1=rand()%(basestrike+level1);
    if(strike0>strike1) nfleets1--;   /// defense wins if equal
    else nfleets0--;
  }  
  if(nfleets0>0) return nfleets0;
  else return -nfleets1; 	
}  

bool Sector::iscentral() {
  if(number==1) return true;
  else return false;
}  
bool Sector::isring() {
  if(number>=2 && number<9) return true;
  else return false;
}  
bool Sector::isbridge() {
  if(number>=9 && number<16) return true;
  else return false;
}  
bool Sector::isperiphery() {
  if(number>=16) return true;
  else return false;
}  

int Sector::victory_points() {
  int points=1;
  if(isring()) points++;
  if(iscentral()) points+=2;
  if(capital!="no") points+=2;
  if(capital!="no" && owner=="Empire") points++;   /// faction bonus
  for(int k=0;k<megastructures.size();k++) points++;
  return points;
}  

int Player::victory_points() {
  int points=0;
  for(int j=0;j<sectors.size();j++) {
    points+=sectors[j]->victory_points();
  }  
  return points;
} 

void Sector::print() {
  cout << "Secteur " << number << " : " << owner << ", " << FF << " flottes ; R+" << gain(0) << " C+" << gain(1) << " S+" << gain(2) << " X+" << gain(3) << " ; ";
  if(megastructures.size()>0) cout << "megastructures ";
  for(int k=0;k<megastructures.size();k++) cout << megastructures[k] << " ";
  cout << " ; " << victory_points() << " pts" << endl;
}  

void Player::print() {
  cout << name << " : " << RR[0] << "R, " << RR[1] << "C, " << RR[2] << "S, " << RR[3] << "X ; sec ";
  for(int jj=0;jj<sectors.size();jj++) {
    cout << sectors[jj]->number;
    if(sectors[jj]->capital==name) cout << " (cap), ";
    else cout << ", ";
  }  
  cout << "; " << "déf " << Tlevel[0]-Tminlevel[0] << ", att " << Tlevel[1]-Tminlevel[1] << ", adm " <<  Tlevel[2]-Tminlevel[2] << ", con " << Tlevel[3]-Tminlevel[3]; 
  cout << " ; vic " << victory_points() << "/" << victory << endl;
}  

vector<int> Player::hyperdrive(vector<Sector> allsectors,int Asector,int Bsector) {
  vector<int> output;
  vector<int> bestpath;
  float minnturns=99999.;
  float nfriend=0.5;
  float nennemy=1.;
  float nturns=0.;
  if(name=="Cardan") nennemy=0.5;
  if(adjsect[0].size()==0) {
    adjsect[1-1].push_back(2-1);adjsect[1-1].push_back(3-1);adjsect[1-1].push_back(4-1);adjsect[1-1].push_back(5-1);adjsect[1-1].push_back(6-1);adjsect[1-1].push_back(7-1);adjsect[1-1].push_back(8-1);
    adjsect[2-1].push_back(1-1);adjsect[2-1].push_back(3-1);adjsect[2-1].push_back(8-1);adjsect[2-1].push_back(9-1);adjsect[2-1].push_back(10-1);
    adjsect[3-1].push_back(1-1);adjsect[3-1].push_back(2-1);adjsect[3-1].push_back(4-1);adjsect[3-1].push_back(10-1);adjsect[3-1].push_back(11-1);
    adjsect[4-1].push_back(1-1);adjsect[4-1].push_back(3-1);adjsect[4-1].push_back(5-1);adjsect[4-1].push_back(11-1);adjsect[4-1].push_back(12-1);
    adjsect[5-1].push_back(1-1);adjsect[5-1].push_back(4-1);adjsect[5-1].push_back(6-1);adjsect[5-1].push_back(12-1);adjsect[5-1].push_back(13-1);
    adjsect[6-1].push_back(1-1);adjsect[6-1].push_back(5-1);adjsect[6-1].push_back(7-1);adjsect[6-1].push_back(13-1);adjsect[6-1].push_back(14-1);
    adjsect[7-1].push_back(1-1);adjsect[7-1].push_back(6-1);adjsect[7-1].push_back(8-1);adjsect[7-1].push_back(14-1);adjsect[7-1].push_back(15-1);
    adjsect[8-1].push_back(1-1);adjsect[8-1].push_back(2-1);adjsect[8-1].push_back(7-1);adjsect[8-1].push_back(9-1);adjsect[8-1].push_back(15-1);
    adjsect[9-1].push_back(2-1);adjsect[9-1].push_back(8-1);adjsect[9-1].push_back(18-1);adjsect[9-1].push_back(19-1);
    adjsect[10-1].push_back(2-1);adjsect[10-1].push_back(3-1);adjsect[10-1].push_back(21-1);adjsect[10-1].push_back(22-1);
    adjsect[11-1].push_back(3-1);adjsect[11-1].push_back(4-1);adjsect[11-1].push_back(24-1);adjsect[11-1].push_back(25-1);
    adjsect[12-1].push_back(4-1);adjsect[12-1].push_back(5-1);adjsect[12-1].push_back(27-1);adjsect[12-1].push_back(28-1);
    adjsect[13-1].push_back(5-1);adjsect[13-1].push_back(6-1);adjsect[13-1].push_back(30-1);adjsect[13-1].push_back(31-1);
    adjsect[14-1].push_back(6-1);adjsect[14-1].push_back(7-1);adjsect[14-1].push_back(33-1);adjsect[14-1].push_back(34-1);
    adjsect[15-1].push_back(7-1);adjsect[15-1].push_back(8-1);adjsect[15-1].push_back(16-1);adjsect[15-1].push_back(36-1);
    adjsect[16-1].push_back(15-1);adjsect[16-1].push_back(17-1);adjsect[16-1].push_back(36-1);
    adjsect[17-1].push_back(16-1);adjsect[17-1].push_back(18-1);
    adjsect[18-1].push_back(9-1);adjsect[18-1].push_back(17-1);adjsect[18-1].push_back(19-1);
    adjsect[19-1].push_back(9-1);adjsect[19-1].push_back(18-1);adjsect[19-1].push_back(20-1);
    adjsect[20-1].push_back(19-1);adjsect[20-1].push_back(21-1);
    adjsect[21-1].push_back(10-1);adjsect[21-1].push_back(20-1);adjsect[21-1].push_back(22-1);
    adjsect[22-1].push_back(10-1);adjsect[22-1].push_back(21-1);adjsect[22-1].push_back(23-1);
    adjsect[23-1].push_back(22-1);adjsect[23-1].push_back(24-1);
    adjsect[24-1].push_back(11-1);adjsect[24-1].push_back(23-1);adjsect[24-1].push_back(25-1);
    adjsect[25-1].push_back(11-1);adjsect[25-1].push_back(24-1);adjsect[25-1].push_back(26-1);
    adjsect[26-1].push_back(25-1);adjsect[26-1].push_back(27-1);
    adjsect[27-1].push_back(12-1);adjsect[27-1].push_back(26-1);adjsect[27-1].push_back(28-1);
    adjsect[28-1].push_back(12-1);adjsect[28-1].push_back(27-1);adjsect[28-1].push_back(29-1);
    adjsect[29-1].push_back(28-1);adjsect[29-1].push_back(30-1);
    adjsect[30-1].push_back(13-1);adjsect[30-1].push_back(29-1);adjsect[30-1].push_back(31-1);
    adjsect[31-1].push_back(13-1);adjsect[31-1].push_back(30-1);adjsect[31-1].push_back(32-1);
    adjsect[32-1].push_back(31-1);adjsect[32-1].push_back(33-1);
    adjsect[33-1].push_back(14-1);adjsect[33-1].push_back(32-1);adjsect[33-1].push_back(34-1);
    adjsect[34-1].push_back(14-1);adjsect[34-1].push_back(33-1);adjsect[34-1].push_back(35-1);
    adjsect[35-1].push_back(34-1);adjsect[35-1].push_back(36-1);
    adjsect[36-1].push_back(15-1);adjsect[36-1].push_back(16-1);adjsect[36-1].push_back(35-1);
  }
  int step=0;
  int current[9999];
  current[0]=Asector-1;
  int steppedup=1;
  int previous=-1;
  vector<int> steptry[9999];
  vector<int> allsteptries[9999];
  bool arrived;
  if(Asector!=Bsector) while(step>-1) {
    if(debug) cout << "step " << step << "(" << current[step]+1 << "): "; 
    arrived=0;
    /// compute number of turns to reach current step
    nturns=0;
    for(int i0=1;i0<=step;i0++) {
      float moreturns=nfriend;
      if(allsectors[current[i0]].owner!=name) moreturns=nennemy;
      nturns+=moreturns;
    }	
    /// fill tries if just stepped up
    if(steppedup) {
      steptry[step]=adjsect[current[step]]; 
      for(int i1=0;i1<steptry[step].size();i1++) {
	allsteptries[step].push_back(steptry[step][i1]);
      }  
      steppedup=0;
    }	
    /// remove tries to sector owned by other player from sector owned by same player
    for(int i1=0;i1<steptry[step].size();i1++) {
      if(allsectors[current[step]].owner!="rebelle" && allsectors[current[step]].owner!=name) {
	if(allsectors[current[step]].owner==allsectors[steptry[step][i1]].owner) {
	  steptry[step].erase(steptry[step].begin()+i1);
	  i1--;
	}  
      }  
    }	      
    /// check if one try is the destination
    for(int i1=0;i1<steptry[step].size();i1++) {
      if(steptry[step][i1]==Bsector-1) {
	steptry[step].clear();
	i1=0;
	if(nturns<minnturns) {
	  minnturns=nturns;
	  bestpath.clear();
	  for(int i0=1;i0<=step;i0++) {
	    bestpath.push_back(current[i0]);
	  }  
	  bestpath.push_back(Bsector-1);
	  if(debug) for(int i0=0;i0<bestpath.size();i0++) cout << current[i0]+1 << "-";
	}  
	arrived=1;
      }  
    }
    /// remove all tries considered in previous steps or corresponding to current in previous steps
    if(step>0) {
      vector<int> toberemoved;
      for(int i1=0;i1<steptry[step].size();i1++) {
	for(int allsteps=0;allsteps<step;allsteps++) {
	  for(int i0=0;i0<allsteptries[allsteps].size();i0++) {	    
	    if(steptry[step][i1]==allsteptries[allsteps][i0] || steptry[step][i1]==current[allsteps]) {
	      toberemoved.push_back(steptry[step][i1]);
	    }  
	  }  
	} 
      }
      for(int i0=0;i0<toberemoved.size();i0++) {
	for(int i1=0;i1<steptry[step].size();i1++) { 	    
	  if(steptry[step][i1]==toberemoved[i0]) {
	    steptry[step].erase(steptry[step].begin()+i1);
	    i1--;
	  }  
	}  
      }  
    }	  
    /// compute nturns for each try and remove tries which give larger nturns than previous minimum
    for(int i1=0;i1<steptry[step].size();i1++) {
      float moreturns=nfriend;
      if(allsectors[steptry[step][i1]].owner!=name) moreturns=nennemy;
      if(nturns+moreturns>=minnturns) {
	steptry[step].erase(steptry[step].begin()+i1);
	i1--;
      }  	
    }  
    /// monitoring
    if(debug) {  
      for(int ii=0;ii<steptry[step].size();ii++) cout << steptry[step][ii]+1 << " ";
      cout << " --- nturns = " << nturns << " (min " << minnturns << ")";
      cout << endl;
    }	
    /// dead end or arrived --> step back
    if(arrived || steptry[step].size()==0) {
      if(step>0) steptry[step-1].erase(steptry[step-1].begin());
      step--;
    }	
    /// otherwise --> step up
    else {
      current[step+1]=steptry[step][0];
      steppedup=1;
      step++;
    }
  }
  else {
    minnturns=0;
    bestpath.push_back(Asector-1);
  }  
  
  float result=ceil(minnturns+0.5);
  if(debug) cout << "resulting number of turns: " << result << endl;
  output.push_back(result);
  for(int i0=0;i0<bestpath.size();i0++) {
    output.push_back(bestpath[i0]);
  }  
  return output;
}  

///////////////////////////////////////////////

int randgain(int mean) {
  int half=(int) floor(mean/2);
  int gain=half+rand()%(2*half);
  return gain;
}  

void scores(vector<Player> players) {

}  



