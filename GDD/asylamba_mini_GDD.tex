\documentclass[a4paper,12pt]{article}
\usepackage{caption}
\usepackage{url}
\usepackage{lastpage}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{array}
\usepackage{fancyhdr}
\usepackage{verbatim}
\usepackage{framed}
\usepackage{mdframed}
\usepackage[top=3cm,bottom=2.5cm]{geometry}
%\usepackage[headheight=110pt, bottom=1.5cm,left=2cm,right=2cm, a4paper]{geometry}
\usepackage{graphicx}
\usepackage[sc]{mathpazo}
\usepackage{pdfpages}
\linespread{1.05} 
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\begin{document}

\sloppy
%\linespread{1.5}

\thispagestyle{empty}

\begin{center}

\includegraphics[width=0.7\linewidth]{./fig/studio_clouzo_logo.png}

\vspace{4cm}

\textbf{
\huge
Game Design Document} \\

\vspace{1cm}

%Breeding \\
\includegraphics[width=0.7\linewidth]{./fig/asylamba.png} \\

\vspace{8.6cm}

Copyright ©2018 by Studio Clouzo

Written by Philippe Mermod

Version 1.3 (May 2, 2019)

\end{center}

\newpage

%\tableofcontents

\normalsize

\section{Design history}

\begin{itemize}
\item[2015] The online multi-player game Asylamba (\url{https://asylamba.com/accueil}) is released and quickly acquires a community of over 1000 players. 
\item[2016] Philippe Mermod from Studio Clouzo produces a first prototype (v0.1, implemented in C++ and playable in text mode) of a mini version following the concept of board games 2.0. The prototype extensively uses the universe and many elements of the game Asylamba and is designed for 2 to 7 players for a game duration around 1 hour. It is tested with Asylamba's developers, who react positively to the idea. 
\item[2017] Several versions (v0.x) of the prototype are produced by Philippe Mermod and tested at evenings organised by Studio Clouzo. A graphical prototype programmed in LabView is produces by Cédric Martin, demonstrating some aspects of the visual rendering and user interface. 
\item[2018] An optimised version (v1.0) is produced by Philippe Mermod, using feedback obtained at previous tests. 
\item[2019] More features including new factions (for 12 in total) are added and optimisation is performed to improve the gameplay and shorten the game duration (v1.3, current version).
\end{itemize}

\section{Game philosophy}

\subsection{Board game 2.0}

The founding concept of the Studio Clouzo company is that of ``board game 2.0'', i.e, turn-based video games designed for several players in the same room, in which the main emphasis is on stimulating exchanges between players. The machine offers clear advantages as compared with regular board games, such as complex in-game calculations to update player status or handle complicated random outcomes, automatic score counting, artificial intelligence (AI), animations, ambience sounds and musics, and game saves. The idea of exploiting such possibilities to create a unique shared experience for people playing together opens a lot of room for innovation. 

\subsection{Asylamba Mini}

Asylamba Mini is based on the universe of the online multi-player game Asylamba and follows the same broad philosophy. Different factions are struggling for galactic dominance. They start with very little and gradually colonise and conquer territories -- constantly faced with a delicate balance between building and sending fleets now and investing in base development to ripe the fruits later. In the original version, hundreds of players control the factions, planets are colonised or conquered by individual players for factions to obtain a majority in each sector of the galaxy, and the game takes several months to complete. The mini version is much simplified, with each player directly controlling a faction and colonising or conquering sectors of the galaxy, for the game to take only about one hour to complete. 

\section{Core game play mechanics}

\subsection{Victory conditions}

The first faction to accumulate 12 victory points wins the game. Victory points are obtained by controlling sectors, with the amount of points given by a sector depending on sector placement (see Section~\ref{sectors}) and other features as detailed in Table~\ref{tab:vic}. 

\begin{minipage}{\textwidth}
  \begin{minipage}[b]{0.4\textwidth}
    \centering
%     \rule{6.4cm}{3.6cm}
    \includegraphics[width=0.9\linewidth]{./fig/galaxy.png}
    \captionof{figure}{Map of the Galaxy's 36 sectors.}    
    \label{fig:gal}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.59\textwidth}
    \centering
    \begin{tabular}{|l|c|}
    \hline
    feature & points \\
    \hline
    peripheral or bridge           & +1 \\
    central ring                   & +2 \\ 
    central                        & +3 \\
    capital owned by Empire        & +1 \\        
    governor sector bonus          & +1 \\
    megastructure                  & +1 \\    
    \hline
    \end{tabular}
    \captionof{table}{Details of the amount of victory points provided by sector ownership and from other sources.}        
    \label{tab:vic}
  \end{minipage}
\end{minipage}

\subsection{Game start}

At the start of the game, one chooses the number of human and AI players (max 7 in total), and each player chooses a faction (see Section~\ref{factions}). Capital sectors are determined randomly among peripheral ones adjacent to a bridge (see Section~\ref{sectors}).

\subsection{Sectors of the Galaxy}
\label{sectors}

The game takes place on the original galactic map of Asylamba as shown in Fig.~\ref{fig:gal}. It comprises 36 sectors, among which one central (1), 7 in the central ring ($2-8$), 7 bridges ($9-15$), and 21 peripheral ($16-36$).  

A sector comprises the facilities shown in Table~\ref{tab:facilities}, whose resource output per turn is proportional to their level. There are four types of resources, ressources (R), credits (C), science (S) and experience (X):
\begin{itemize}
\item R is a local sector resource produced by the refinery and needed to level up the refinery and build fleets and megastructures. 
\item C is a global faction resource produced by the commercial hub and needed to level up the commercial hub, colonise new sectors, and research elementary technologies. 
\item S is a global faction resource produced by the technosphere and needed to research strategic technologies. 
\item X is a global faction resource produced by the military school and by winning combats, and needed to conquer sectors and research the conquest technology. 
\end{itemize}

\begin{table}
\centering
\begin{tabular}{|l|c|c|c|c|}
  \hline
  facility & start level & max level & output/level & upgrade cost/level  \\
  \hline
  refinery        & 4 & 25 & 10 R & 10 R            \\
  commercial hub  & 4 & 25 & 10 C & 10 C            \\ 
  technosphere    & 0 & 25 & 10 S & cannot upgrade  \\
  military school & 0 & 25 & 10 X & cannot upgrade  \\        
  \hline
\end{tabular}
\caption{Summary of possible sector facilities and their outputs. The indicated start level is for capital sectors; for rebel sector it is 0 for all facilities -- however these are the defaults, and can be increased by sector bonuses. Since the technosphere and military school cannot be upgraded in the capital, the only way to obtain S or X is through bonuses or combat.}
\label{tab:facilities}
\end{table}

All sectors start as "rebels" with all facilities at level 0, and are given a number of defending fleets. Capital sectors have the refinery and commercial hub at level 4, and 4 fleets by default. Rebel sectors have a special bonus, which is hidden until the sector is colonised. The special bonuses are chosen from the list below, which details their effects. 
\begin{itemize}
\item recycling centre: refinery level +2
\item trade route: commercial hub level +2
\item university: technosphere level +2
\item état-major: military school level +2
\item royal guard: +20 fleets (10 medium, 10 heavy; single usage)
\item treasure: +200~C (single usage)
\item library: +200~S (single usage)
\item commandant: +200~X (single usage)
\item laboratory: free technology (single usage)
%\item minotaure: +10\% defence faction-wide
%\item hydre: +10\% attack faction-wide
\item spy: free colonisation (single usage)
\item general: free conquest (single usage)
\item governor: +1 victory point
\end{itemize}  

More central sectors, as well as sectors with military-based bonuses, have a larger amount of starting fleets. Sectors with science-based bonuses may have more advanced fleet types. Sectors with recycling centres automatically build fleets every turn. 

\subsection{Technologies}

The technologies are summarised in Table~\ref{tab:tech}. Their cost is proportional to their level. The most important one is the extended administration (which costs C), as it allows for one more colonisation or conquest per level -- thus the maximum number of sectors owned by a faction beyond the capital (essential for accumulating victory points) is equal to its level. The spatial engineering technology (which also costs C) unlocks the construction of megastructures (see Section~\ref{megastructures}) and thus offers an alternative way to gain victory points. 

\begin{table}
\centering
\begin{tabular}{|l|c|c|c|c|}
  \hline
  technology              & max level & cost/level & unlocks \\
  \hline
  extended administration & 11        & 50 C       & new sector ownership \\
  spatial engineering     &  5        & 50 C       & megastructure construction \\              
  conquest                &  1        & 200 X      & conquest missions \\
  defence                 & 10        & 50 S       & +10\% defence per level \\ 
  attack                  & 10        & 50 S       & +10\% attack per level \\ 
  frame                   &  2        & 100 S      & heavier battleships \\ 
  \hline
\end{tabular}
\caption{Summary of technologies and their costs and uses.}
\label{tab:tech}
\end{table}

\subsection{Factions}
\label{factions}

There are 12 different factions corresponding to those of the original Asylamba game, which grant specific bonuses, as detailed below. 
\begin{itemize}
\item Empire (bright red): +1 victory point in each capital.
\item Nerve (bright green): +30\% defence strength, and refinery and technsphere +1 in capital for each successful sector defence. 
\item Negore (bright yellow): can exchange resources with itself at a rate of 1/2. 
\item Aphera (bright cyan): technology costs reduced by 25\%.
\item Cardan (bright magenta): double movement speed in neutral or enemy territory.
\item Synelle (dark blue): starts with technosphere, defence, attack, extended administration, and spatial engineering technologies at level 1. 
\item Kovahk (bright blue): when winning combats, gain triple X and capture 1/4 of enemy fleets. 
\item Magoth (dark magenta): starts with conquest technology and military school at level 1.
\item Imperialistes (dark yellow): for each successful conquest: military school +3 in capital and attack +1.
\item Nouvelle-Nerve (dark green): megastructure costs reduced by 30\%. 
\item Neo-Humaniste (dark cyan): colonisation cost reduced by 30\%, and +30\% attack for colonisations. 
\item Ligue (dark red): can purchase fleets with C.
\end{itemize}

%Factions can freely exchange resources (C, S, X and fleets). 
Factions can exchange one resource for another with themselves at a rate of 1/4 (1/2 for Negore). 

\subsection{Fleets, missions and combat}
\label{combat}

There are three types of ships: light, medium (unlocked by frame technology at level 1) and heavy (unlocked by frame technology at level 2). Medium ships gain +25\% combat (for both attack and defence) against light ships, the same for heavy against medium, and for light against heavy. Fleets of a given type of ships can be built in one turn in the capital at the cost of 10~R per fleet (or 10~C per fleet for the Ligue), and are automatically built in non-capital sectors with refineries. 

A group of any number of fleets can be defined and sent from a sector to another to perform a specific mission. A faction can have at most twice as many missions as sectors at any given time.
%There is an option to redirect an ongoing mission with a new destination and a new type. 
There are 3 different mission types: 
\begin{itemize}
\item Reinforcements: move defensive fleets between two sectors owned by the fleet's faction.
\item Colonisation: attack of a rebel sector. This costs 50~C times the number of already owned sectors, and requires the extended administration technology to the appropriate level. 
\item Conquest: attack of a sector owned by another faction. This costs 50~X times the number of already owned sectors, and requires the conquest technology as well as extended administration to the appropriate level. 
%\item Patrol: stationary placement of fleets in a rebel sector. 
%\item Interception: ongoing mission is ordered to attack enemy fleets which are currently on a mission in the same sector. 
\end{itemize}

The mission path is computed automatically such as to minimise the number of turns to get to the destination. Fleets generally travel at a speed of one sector per turn; however if they move from one friendly sector to another friendly sector, they travel at the speed of two sectors per turn. Cardan fleets travel two sectors per turn regardless. A fleet cannot take a path that makes it move from one enemy sector to another enemy sector. 

The combat takes place as a series of skirmishes between two randomly chosen squadrons (each fleet having 3 squadrons). In each skirmish, the outcome probability depends on attack and defence modifiers, and the losing squadron is destroyed. These skirmishes continue until one of the combatant has no fleets left. The winner keeps its remaining fleets (as well as 1/3 of the defeated fleets for Kovahk) and wins 10~X (30~X for Kovahk) for each destroyed fleet. Upon successful defence, nothing happens (except Nerve which gains 200~S). In case of successful attack, the sector is captured by the attacker (and 300~C is gained for Imperialistes). If an enemy capital is captured, the corresponding faction is eliminated from the game and all its remaining sectors become rebels. 

\subsection{Megastructures}
\label{megastructures}

The megastructures are galaxy wonders which can each be built only once. A megastructure is produced in a capital sector, and provides +1 victory point in that sector. It can only be built after researching the spatial engineering technology at the required level and it costs an increasing amount of R for increasing level. Each megastructure provides a unique bonus: 
\begin{itemize}
\item Space elevator (level 1, cost 150~R): refinery +2. 
\item Gravity train (level 1, cost 150~R): commercial hub +2. 
\item Globus Cassus (level 2, cost 200~R): military school +1 and +20\% defence faction-wide. 
\item Klemperer Rosette (level 2, cost 200~R): military school +1 and +20\% attack faction-wide.  
\item Circumsolar collider (level 3, cost 250~R): technosphere +2 and one free technology.  
\item Planet Gaia (level 3), cost 250~R: extended administration +1 and one free colonisation or conquest.
\item Bernal Sphere (level 4, cost 300~R): fleets doubled in this sector.   
\item Ringworld (level 4, cost 300~R): commercial hub at maximum level.  
\item Dyson Sphere (level 5, cost 400~R): refinery at maximum level.    
\end{itemize}

\section{User interface}

Below are some suggestions about how we envisage the game to be developed in graphics mode.  

\subsection{Platforms}

The aim is for the game to be playable on a tablet computer and PC, and perhaps later on a game console and smartphone. Being able to implement mouseovers would make the game more intuitive. 

\subsection{Player views and actions}

Like Asylamba, we will adopt a two-dimensional top view of the galaxy and sectors. The main view will be a map of the galaxy with sectors highlighted by colour with the capital sector in a brighter tone -- the visual rendering will be similar to Fig.~\ref{fig:galaxymap}, taken from a game of Asylamba. An indicator of the number of fleets stationed in each sector should be visible. Also, all current missions will be displayed as coloured lines between sectors with an icon for the type of mission and an indicator of the number of fleets. More detailed information about a mission or a sector could be obtained by hovering. Missions can be launched by selecting own fleets on this view. 

\begin{figure}[tb]
\centering
  \includegraphics[width=0.8\linewidth]{./fig/sectors/galaxy_raw.png}
  \caption{Main view of the galaxy with faction territories highlighted.}    
  \label{fig:galaxymap}
\end{figure}

By clicking on a sector, one would zoom into it and get the sector view. The sector view shows the facilities and their level, the fleets, and the refinery and commercial hub possible upgrades. It also displays the technology levels and possible upgrades of the faction owning the sector as well as faction resources and victory points. The sector bonus and possible megastructures should also be clearly visible on this view. 

By default one gets the view of the capital sector at the beginning of each turn. Upgrades and building of fleets and megastructures can be defined from this view. A trade dialog can also be opened from this view by clicking on a resource. 

\subsection{Graphics and animations}

Graphics are borrowed or adapted from Asylamba as much as possible. Below is a sample of graphics which we can readily use and tentative association with elements of Asylamba Mini. 

\subsection{Sounds and music}

While Asylamba is a silent game, we envisage developing special sounds and musics for Asylamba Mini to enhance its particular atmosphere. 

\begin{minipage}{\textwidth}
  \begin{minipage}[b]{0.5\textwidth}
   
    \begin{itemize}
      \item Empire: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Empire.png}
      \item Cardan: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Cardan.png} 
      \item Nerve: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Nerve.png}  
      \item Synelle: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Synelle.png}  
      \item Kovahk: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Kovahk.png}  
      \item Resources:  \includegraphics[width=0.2\linewidth]{./fig/construction/resource.png}  
      \item Credits:  \includegraphics[width=0.2\linewidth]{./fig/construction/credit.png}            
     \end{itemize}
   
  \end{minipage}
  \begin{minipage}[b]{0.5\textwidth}

    \begin{itemize}
      \item Negore: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Negore.png}  
      \item Aphera: \includegraphics[width=0.2\linewidth]{./fig/sectors/flag-Aphera.png}  
      \item Base type 0: \includegraphics[width=0.2\linewidth]{./fig/sectors/base-type-0.jpg}
      \item Base type 1: \includegraphics[width=0.2\linewidth]{./fig/sectors/base-type-1.jpg}
      \item Base type 2: \includegraphics[width=0.2\linewidth]{./fig/sectors/base-type-2.jpg}
      \item Base type 3: \includegraphics[width=0.2\linewidth]{./fig/sectors/base-type-3.jpg}         
      \item Science:  \includegraphics[width=0.2\linewidth]{./fig/construction/science.png}   
      \item Experience: \includegraphics[width=0.2\linewidth]{./fig/construction/xp.png} 
    \end{itemize}

  \end{minipage}  
\end{minipage}


\begin{minipage}{\textwidth}
  \begin{minipage}[b]{0.5\textwidth}
   
    \begin{itemize}
      \item Fleets: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/missions/army.png} \end{mdframed} 
      \item Colo mission: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/missions/colo.png} \end{mdframed} 
      \item Conquest mission: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/missions/loot.png} \end{mdframed} 
      \item Reinforcement mission: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/missions/move.png} \end{mdframed} 
      \item Defending fleets: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/missions/shield.png} \end{mdframed}                 
      \item Generator: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/generator.png} \end{mdframed} 
      \item Refinery: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/refinery.png} \end{mdframed}
      \item Commercial hub: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/commercialplateforme.png} \end{mdframed}
    \end{itemize}
   
  \end{minipage}
  \begin{minipage}[b]{0.5\textwidth}
    \begin{itemize}
      \item Technosphere: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/technosphere.png} \end{mdframed}
      \item Shipyard: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/dock2.png} \end{mdframed}
      \item Tech defence: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/corvettedefense.png} \end{mdframed}
      \item Tech attack: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/corvetteattack.png} \end{mdframed}
      \item Tech extended administration: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/colonization.png} \end{mdframed}
      \item Tech conquest: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/conquest.png} \end{mdframed}
      \item Tech spacial engineering: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/construction/gravitmodule.png} \end{mdframed}
      \item University: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/sectors/university.png} \end{mdframed}                 
      \item Spaceport: \begin{mdframed}[backgroundcolor=black] \includegraphics[width=0.2\linewidth]{./fig/sectors/spatioport.png} \end{mdframed}                 
    \end{itemize}

  \end{minipage}  
\end{minipage}



\end{document}



