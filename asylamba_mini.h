using namespace std;

//////////////////////////////////////////////////

class chainlink {
public:
    int index;
    int level;
    int turns;
};  

class Megastructure {
public:
    int number;
    string name;
    string bonus;
    int cost;
    int level;
    int nturns;
    int inconstruction;
    int sector;
    int usedbonus;
};

class Fleet {
public:
    bool isl=0;  /// is light
    bool ism=0;  /// is medium
    bool ish=0;  /// is heavy
};

class Sector {
public:
    int number;
    string owner;
    string capital;
    vector<Fleet> FF;      // fleets
       
    int Rbonus[4];  // bonus 0:resources, 1:credits, 2:science, 3:experience
    int totbonus();
    int RR;         // resources
    int Glevel[4];  // level 0:raffinery, 1:industry, 2:technosphere, 3:military
    vector<Megastructure*> megastructures;
    int gain(int resourceindex);
    vector<chainlink> Gchain;   /// generator construction chain
    vector<chainlink> Tchain;   /// technosphere chain
    vector<chainlink> Fchain;   /// fleet construction chain
    
    bool iscentral();
    bool isring();
    bool isbridge();
    bool isperiphery();
    
    float extradef;
    float extraatt;    
    int extrapoints;       
    int victory_points();
    void print();
    
    string special;
    void getspecialupgrades();
    
};  

class Mission {
public:  
    int number;
    int Asector;   // provenance
    int Bsector;   // destination
    int Csector;   // current
    vector<Fleet> FF;      // fleets
    int nturns;
    int type;      // 0:reinforcements, 1:colonisation, 2:conquest, 3:raid, 4:patrol, 100+missionnumber:interception
    int outgoing;
    
    void print(vector<Sector>);
};  

class Player {
public:
    string name;   // faction name
    
    int isai;
    
    vector<Sector*> sectors;  
    
    int RR[4];  // 0:resources (obsolete -- now sector-based), 1:credits, 2:science, 3:experience
    
    int Tlevel[nTech];   // 0:defense, 1:attaque, 2:admin étendue, 3:conquête, 4:architecture, 5:châssis 
    
    vector<Mission> missions;
    
    vector<int> hyperdrive(vector<Sector>,int,int);  // returns nb of turns and best path in a vector
    
    int freetech;
    int freecolo;
    int freeconq;
    int freecoloconq;
    float extradef();
    float extraatt();        
    void getspecial(int);    
        
    int victory_points();
    void print(int);
    
    string AIgen();
    string AItech();
    int AIfleets();
    string AIoffers();
    int AIoffershowmuch(int);
    string AIwants();    
    int AImissionA();
    int AImissionB(vector<Sector>,int);
    float AImissionhowmuch(vector<Sector>,int,int);
    string AImissiontype(int,int,int);
    
private:
    vector<int> adjsect[36];    
};  

class Thegame {
  public:
  int turn;
  vector<Player> players;
  vector<Sector> sectors;
};  

int Fn(vector<Fleet> FF,int frame) {
   int N=0;
   for(int f=0;f<FF.size();f++) {
     if(frame==0 && FF[f].isl) N++;
     if(frame==1 && FF[f].ism) N++;     
     if(frame==2 && FF[f].ish) N++;          
   }
   return N;
}  

int Sector::totbonus() {
   return Rbonus[0]+Rbonus[1]+Rbonus[2]+Rbonus[3];
}  

int Sector::gain(int resourceindex) {
  int gain=Rbonus[resourceindex]*(Glevel[resourceindex]);
  return gain;
}

vector<int> Player::hyperdrive(vector<Sector> allsectors,int Asector,int Bsector) {
  vector<int> output;
  vector<int> bestpath;
  float minnturns=99999.;
  float nfriend=0.5;
  float nennemy=1.;
  float nturns=0.;
  if(name=="Cardan") nennemy=0.5;  //// faction bonus
  if(adjsect[0].size()==0) {
    adjsect[1-1].push_back(2-1);adjsect[1-1].push_back(3-1);adjsect[1-1].push_back(4-1);adjsect[1-1].push_back(5-1);adjsect[1-1].push_back(6-1);adjsect[1-1].push_back(7-1);adjsect[1-1].push_back(8-1);
    adjsect[2-1].push_back(1-1);adjsect[2-1].push_back(3-1);adjsect[2-1].push_back(8-1);adjsect[2-1].push_back(9-1);adjsect[2-1].push_back(10-1);
    adjsect[3-1].push_back(1-1);adjsect[3-1].push_back(2-1);adjsect[3-1].push_back(4-1);adjsect[3-1].push_back(10-1);adjsect[3-1].push_back(11-1);
    adjsect[4-1].push_back(1-1);adjsect[4-1].push_back(3-1);adjsect[4-1].push_back(5-1);adjsect[4-1].push_back(11-1);adjsect[4-1].push_back(12-1);
    adjsect[5-1].push_back(1-1);adjsect[5-1].push_back(4-1);adjsect[5-1].push_back(6-1);adjsect[5-1].push_back(12-1);adjsect[5-1].push_back(13-1);
    adjsect[6-1].push_back(1-1);adjsect[6-1].push_back(5-1);adjsect[6-1].push_back(7-1);adjsect[6-1].push_back(13-1);adjsect[6-1].push_back(14-1);
    adjsect[7-1].push_back(1-1);adjsect[7-1].push_back(6-1);adjsect[7-1].push_back(8-1);adjsect[7-1].push_back(14-1);adjsect[7-1].push_back(15-1);
    adjsect[8-1].push_back(1-1);adjsect[8-1].push_back(2-1);adjsect[8-1].push_back(7-1);adjsect[8-1].push_back(9-1);adjsect[8-1].push_back(15-1);
    adjsect[9-1].push_back(2-1);adjsect[9-1].push_back(8-1);adjsect[9-1].push_back(18-1);adjsect[9-1].push_back(19-1);
    adjsect[10-1].push_back(2-1);adjsect[10-1].push_back(3-1);adjsect[10-1].push_back(21-1);adjsect[10-1].push_back(22-1);
    adjsect[11-1].push_back(3-1);adjsect[11-1].push_back(4-1);adjsect[11-1].push_back(24-1);adjsect[11-1].push_back(25-1);
    adjsect[12-1].push_back(4-1);adjsect[12-1].push_back(5-1);adjsect[12-1].push_back(27-1);adjsect[12-1].push_back(28-1);
    adjsect[13-1].push_back(5-1);adjsect[13-1].push_back(6-1);adjsect[13-1].push_back(30-1);adjsect[13-1].push_back(31-1);
    adjsect[14-1].push_back(6-1);adjsect[14-1].push_back(7-1);adjsect[14-1].push_back(33-1);adjsect[14-1].push_back(34-1);
    adjsect[15-1].push_back(7-1);adjsect[15-1].push_back(8-1);adjsect[15-1].push_back(16-1);adjsect[15-1].push_back(36-1);
    adjsect[16-1].push_back(15-1);adjsect[16-1].push_back(17-1);adjsect[16-1].push_back(36-1);
    adjsect[17-1].push_back(16-1);adjsect[17-1].push_back(18-1);
    adjsect[18-1].push_back(9-1);adjsect[18-1].push_back(17-1);adjsect[18-1].push_back(19-1);
    adjsect[19-1].push_back(9-1);adjsect[19-1].push_back(18-1);adjsect[19-1].push_back(20-1);
    adjsect[20-1].push_back(19-1);adjsect[20-1].push_back(21-1);
    adjsect[21-1].push_back(10-1);adjsect[21-1].push_back(20-1);adjsect[21-1].push_back(22-1);
    adjsect[22-1].push_back(10-1);adjsect[22-1].push_back(21-1);adjsect[22-1].push_back(23-1);
    adjsect[23-1].push_back(22-1);adjsect[23-1].push_back(24-1);
    adjsect[24-1].push_back(11-1);adjsect[24-1].push_back(23-1);adjsect[24-1].push_back(25-1);
    adjsect[25-1].push_back(11-1);adjsect[25-1].push_back(24-1);adjsect[25-1].push_back(26-1);
    adjsect[26-1].push_back(25-1);adjsect[26-1].push_back(27-1);
    adjsect[27-1].push_back(12-1);adjsect[27-1].push_back(26-1);adjsect[27-1].push_back(28-1);
    adjsect[28-1].push_back(12-1);adjsect[28-1].push_back(27-1);adjsect[28-1].push_back(29-1);
    adjsect[29-1].push_back(28-1);adjsect[29-1].push_back(30-1);
    adjsect[30-1].push_back(13-1);adjsect[30-1].push_back(29-1);adjsect[30-1].push_back(31-1);
    adjsect[31-1].push_back(13-1);adjsect[31-1].push_back(30-1);adjsect[31-1].push_back(32-1);
    adjsect[32-1].push_back(31-1);adjsect[32-1].push_back(33-1);
    adjsect[33-1].push_back(14-1);adjsect[33-1].push_back(32-1);adjsect[33-1].push_back(34-1);
    adjsect[34-1].push_back(14-1);adjsect[34-1].push_back(33-1);adjsect[34-1].push_back(35-1);
    adjsect[35-1].push_back(34-1);adjsect[35-1].push_back(36-1);
    adjsect[36-1].push_back(15-1);adjsect[36-1].push_back(16-1);adjsect[36-1].push_back(35-1);
  }
  int step=0;
  int current[9999];
  current[0]=Asector-1;
  int steppedup=1;
  int previous=-1;
  vector<int> steptry[9999];
  vector<int> allsteptries[9999];
  bool arrived;
  if(debug) cout << "hyperdrive from sector " << Asector << " to sector " << Bsector << ":" << endl;
  if(Asector!=Bsector) while(step>-1) {
    if(debug>1) cout << "step " << step << "(" << current[step]+1 << "): "; 
    arrived=0;
    /// compute number of turns to reach current step
    nturns=0;
    for(int i0=1;i0<=step;i0++) {
      float moreturns=nfriend;
      if(allsectors[current[i0]].owner!=name) moreturns=nennemy;
      nturns+=moreturns;
    }	
    /// fill tries if just stepped up
    if(steppedup) {
      steptry[step]=adjsect[current[step]]; 
      for(int i1=0;i1<steptry[step].size();i1++) {
	allsteptries[step].push_back(steptry[step][i1]);
      }  
      steppedup=0;
    }	
    /// remove tries to sector owned by other player from sector owned by same player
    for(int i1=0;i1<steptry[step].size();i1++) {
      if(allsectors[current[step]].owner!="rebelle" && allsectors[current[step]].owner!=name) {
	if(allsectors[current[step]].owner==allsectors[steptry[step][i1]].owner) {
	  steptry[step].erase(steptry[step].begin()+i1);
	  i1--;
	}  
      }  
    }	      
    /// check if one try is the destination
    for(int i1=0;i1<steptry[step].size();i1++) {
      if(steptry[step][i1]==Bsector-1) {
	steptry[step].clear();
	i1=0;
	if(nturns<minnturns) {
	  minnturns=nturns;
	  bestpath.clear();
	  for(int i0=1;i0<=step;i0++) {
	    bestpath.push_back(current[i0]);
	  }  
	  bestpath.push_back(Bsector-1);
	  if(debug>1) for(int i0=0;i0<bestpath.size();i0++) cout << current[i0]+1 << "-";
	}  
	arrived=1;
      }  
    }
    /// remove all tries considered in previous steps or corresponding to current in previous steps
    if(step>0) {
      vector<int> toberemoved;
      for(int i1=0;i1<steptry[step].size();i1++) {
	for(int allsteps=0;allsteps<step;allsteps++) {
	  for(int i0=0;i0<allsteptries[allsteps].size();i0++) {	    
	    if(steptry[step][i1]==allsteptries[allsteps][i0] || steptry[step][i1]==current[allsteps]) {
	      toberemoved.push_back(steptry[step][i1]);
	    }  
	  }  
	} 
      }
      for(int i0=0;i0<toberemoved.size();i0++) {
	for(int i1=0;i1<steptry[step].size();i1++) { 	    
	  if(steptry[step][i1]==toberemoved[i0]) {
	    steptry[step].erase(steptry[step].begin()+i1);
	    i1--;
	  }  
	}  
      }  
    }	  
    /// compute nturns for each try and remove tries which give larger nturns than previous minimum
    for(int i1=0;i1<steptry[step].size();i1++) {
      float moreturns=nfriend;
      if(allsectors[steptry[step][i1]].owner!=name) moreturns=nennemy;
      if(nturns+moreturns>=minnturns) {
	steptry[step].erase(steptry[step].begin()+i1);
	i1--;
      }  	
    }  
    /// monitoring
    if(debug>1) {  
      for(int ii=0;ii<steptry[step].size();ii++) cout << steptry[step][ii]+1 << " ";
      cout << " --- nturns = " << nturns << " (min " << minnturns << ")";
      cout << endl;
    }	
    /// dead end or arrived --> step back
    if(arrived || steptry[step].size()==0) {
      if(step>0) steptry[step-1].erase(steptry[step-1].begin());
      step--;
    }	
    /// otherwise --> step up
    else {
      current[step+1]=steptry[step][0];
      steppedup=1;
      step++;
    }
  }
  else {
    minnturns=0;
    bestpath.push_back(Asector-1);
  }  
  
  float result=ceil(minnturns+0.5);
  if(debug) cout << "resulting number of turns: " << result << endl;
  output.push_back(result);
  for(int i0=0;i0<bestpath.size();i0++) {
    output.push_back(bestpath[i0]);
    if(debug) cout << "path: " << bestpath[i0] << endl;
  }  
  return output;
}  

vector<int> battle(vector<Fleet> f0,int level0,vector<Fleet> f1,int level1) {
  vector<int> out;
  vector<Fleet> myf0;
  vector<Fleet> myf1;
  for(int i=0;i<3;i++) {
    out.push_back(0);
  }  
  for(int m=0;m<multifleets;m++) {   /// multiply number of fleets to even out the odds
    for(int f=0;f<f0.size();f++) {
      Fleet newfleet;
      newfleet.isl=f0[f].isl;
      newfleet.ism=f0[f].ism;
      newfleet.ish=f0[f].ish;
      myf0.push_back(newfleet);            
    }
    for(int f=0;f<f1.size();f++) {
      Fleet newfleet;
      newfleet.isl=f1[f].isl;
      newfleet.ism=f1[f].ism;
      newfleet.ish=f1[f].ish;
      myf1.push_back(newfleet);            
    }    
  }
  while(myf0.size()>0 && myf1.size()>0) {
    int fbonus0=0;
    int fbonus1=0;
    int index0=rand()%(myf0.size());
    int index1=rand()%(myf1.size());
    if(myf0[index0].ism && myf1[index1].isl) fbonus0=framebonus; 
    if(myf1[index1].ism && myf0[index0].isl) fbonus1=framebonus;       
    if(myf0[index0].ish && myf1[index1].ism) fbonus0=framebonus; 
    if(myf1[index1].ish && myf0[index0].ism) fbonus1=framebonus;       
    if(myf0[index0].isl && myf1[index1].ish) fbonus0=framebonus; 
    if(myf1[index1].isl && myf0[index0].ish) fbonus1=framebonus;     
    int strike0=rand()%(100+level0+fbonus0);
    int strike1=rand()%(100+level1+fbonus1);
    if(strike0>strike1) myf1.erase(myf1.begin()+index1);
    else myf0.erase(myf0.begin()+index0);   /// defense wins if equal
  }  
  if(myf0.size()) {   /// attack wins battle
    int maxf=ceil(myf0.size()/multifleets);
    for(int f=0;f<maxf;f++) {
      if(myf0[f].isl) out[0]++;
      if(myf0[f].ism) out[1]++;
      if(myf0[f].ish) out[2]++;            
    }
  }  
  else {    /// defense wins battle
    int maxf=ceil(myf1.size()/multifleets);  
    for(int f=0;f<maxf;f++) {
      if(myf1[f].isl) out[0]--;
      if(myf1[f].ism) out[1]--;
      if(myf1[f].ish) out[2]--;            
    }
  }
  return out;
}  

void fleetattack(Thegame *game,int i, int m) {
  int Bsectorindex=game->players[i].missions[m].Bsector-1;
  vector<Fleet> attFF=game->players[i].missions[m].FF;
  vector<Fleet> defFF=game->sectors[Bsectorindex].FF;
  int defpindex=-1;         /// index of player under attack, -1 if rebel
  int defmindex=-1;         /// index of mission under attack, -1 if not inteception mission
  int defsindex=-1;         /// player-index of sector under attack, -1 if inteception mission
  int deflevel=0;
  int attlevel=game->players[i].Tlevel[1]*combatlevel+game->players[i].extraatt();
  string defname="rebelle";
  cout << "... bataille !!! ";
  if(game->players[i].missions[m].type<100) {     /// sector attack mission
    if(game->sectors[Bsectorindex].owner!="rebelle") {  /// sector owned by another player
      for(int ii=0;ii<game->players.size();ii++) {
	      if(game->players[ii].name==game->sectors[Bsectorindex].owner) {
	        defpindex=ii;
	        deflevel=game->players[ii].Tlevel[0]*combatlevel+game->players[ii].extradef();
	        for(int jj=0;jj<game->players[ii].sectors.size();jj++) {    
	          if(game->players[ii].sectors[jj]->number==game->sectors[Bsectorindex].number) defsindex=jj;
	        }
	      }
      }  
    }          
    else {  /// rebel sector
      if(game->players[i].name=="Neo-Humaniste") attlevel+=neobonus2;  /// faction bonus
    }
    cout << Fn(attFF,0) << "/" << Fn(attFF,1) << "/" << Fn(attFF,2) << " flottes (+" << attlevel << "%) attaquent le secteur " << game->sectors[Bsectorindex].number << " (";
    if(defpindex>-1) cout << game->players[defpindex].name; 
    else cout << "rebelle"; 
    cout << ", " << Fn(defFF,0) << "/" << Fn(defFF,1) << "/" << Fn(defFF,2) << " flottes, +" << deflevel << "%)" << endl;
  }
  int Xgain=Xgainperfleet*defFF.size();
  int defXgain=Xgainperfleet*attFF.size();
  int Fgain=0;  
  int Fgain0=0;  
  int Fgain1=0;  
  int Fgain2=0;        
  int defFgain=0;
  int defFgain0=0;
  int defFgain1=0;
  int defFgain2=0;      
  if(game->players[i].name=="Kovahk") {
    Xgain=kovahkbonus1*Xgain;           /// faction bonus
    Fgain=kovahkbonus2*defFF.size();    /// faction bonus
  }  
  if(defpindex>-1) if(game->players[defpindex].name=="Kovahk") {
    defXgain=kovahkbonus1*defXgain;   /// faction bonus
    defFgain=kovahkbonus2*attFF.size();      /// faction bonus
  }
  vector<int> outcome=battle(attFF,attlevel,defFF,deflevel);
  if(outcome[0]>0 || outcome[1]>0 || outcome[2]>0) {
    if(game->players[i].missions[m].type==3) {   /// pillage
      cout << "... attaque réussie ! secteur " << game->players[i].missions[m].Bsector << " pillé par " << game->players[i].name << " (" << outcome[0] << "/" << outcome[1] << "/" << outcome[2] << " flottes restantes, +" << Xgain << "X";
      if(Fgain) cout << ", " << Fgain << " flottes capturées";		
      cout << ")" << endl; 
    }
    else {
      /// colonisation / conquest
      cout << "... attaque réussie ! secteur " << game->players[i].missions[m].Bsector << " capturé par " << game->players[i].name << " (" << outcome[0] << "/" << outcome[1] << "/" << outcome[2] << " flottes restantes, +" << Xgain << "X";
      if(Fgain) cout << ", " << Fgain << " flottes capturées";		
      cout << ")" << endl; 
      game->sectors[Bsectorindex].owner=game->players[i].name;
    }
    while(Fgain) {   /// captured fleets
      if(Fn(defFF,2)>=Fgain) {   /// preferentially heavy
        Fgain2++;
        Fgain--;        
      }
      else if(Fn(defFF,1)+Fn(defFF,2)>=Fgain) {
        Fgain1++;
        Fgain--;        
      }
      else {
        Fgain0++;
        Fgain--;        
      }
    }    
    game->sectors[Bsectorindex].RR=0;
    game->players[i].RR[3]+=Xgain;
    game->sectors[Bsectorindex].FF.clear();
    
    if(game->players[i].missions[m].type==3) {   /// pillage
      /// fleets are sent back
      int mysector=game->players[i].missions[m].Bsector;
      game->players[i].missions[m].Bsector=game->players[i].missions[m].Asector;
	    game->players[i].missions[m].Asector=mysector;
	    game->players[i].missions[m].Csector=mysector;
	    game->players[i].missions[m].type=0;
	    game->players[i].missions[m].outgoing=0;
	    vector<int> hyper=game->players[i].hyperdrive(game->sectors,game->players[i].missions[m].Asector,game->players[i].missions[m].Bsector);
	    game->players[i].missions[m].nturns=hyper[0];	 
      game->players[i].missions[m].FF.clear(); 
      for(int f=0;f<outcome[0]+Fgain0;f++) {
        Fleet newfleet;
        newfleet.isl=1;
        game->players[i].missions[m].FF.push_back(newfleet); 
      }
      for(int f=0;f<outcome[1]+Fgain1;f++) {
        Fleet newfleet;
        newfleet.ism=1;
        game->players[i].missions[m].FF.push_back(newfleet); 
      }
      for(int f=0;f<outcome[2]+Fgain2;f++) {
        Fleet newfleet;
        newfleet.ish=1;
        game->players[i].missions[m].FF.push_back(newfleet); 
      }
    }
    else {   /// conquest/colo
      /// fleets stay in this sector
      for(int f=0;f<outcome[0]+Fgain0;f++) {
        Fleet newfleet;
        newfleet.isl=1;
        game->sectors[Bsectorindex].FF.push_back(newfleet); 
      }
      for(int f=0;f<outcome[1]+Fgain1;f++) {
        Fleet newfleet;
        newfleet.ism=1;
        game->sectors[Bsectorindex].FF.push_back(newfleet); 
      }
      for(int f=0;f<outcome[2]+Fgain2;f++) {
        Fleet newfleet;
        newfleet.ish=1;
        game->sectors[Bsectorindex].FF.push_back(newfleet); 
      }
      game->players[i].sectors.push_back(&game->sectors[Bsectorindex]);
      if(defpindex>-1) {     /// conquest
        game->players[defpindex].sectors[defsindex]->Fchain.clear();
        game->players[defpindex].sectors[defsindex]->Gchain.clear();	        
        game->players[defpindex].sectors[defsindex]->Tchain.clear();	        	        
        if(defsindex==0 && game->players[defpindex].sectors[defsindex]->capital==game->players[defpindex].name) { /// conquered capital sector
          cout << "... " << game->players[defpindex].name << " se fait prendre sa capitale par " << game->players[i].name << " !!!" << endl;
          cout << "... " << game->players[defpindex].name << " est éliminée du jeu..." << endl;
          for(int jj=0;jj<game->players[defpindex].sectors.size();jj++) {
            if(game->players[defpindex].sectors[jj]->owner==game->players[defpindex].name) {
              game->players[defpindex].sectors[jj]->owner="rebelle";
            }  
          } 
          game->players[defpindex].sectors.clear();
          game->players[defpindex].missions.clear();
        }  
        else {
          game->players[defpindex].sectors.erase(game->players[defpindex].sectors.begin()+defsindex);
        }  
        if(game->players[i].name=="Imperialistes") {   /// faction bonus
          if(game->players[i].sectors[0]->capital==game->players[i].name) game->players[i].sectors[0]->Glevel[3]+=4;
          else cout << "ERROR: wrong sector index for capital sector" << endl;
          game->players[i].Tlevel[1]++;
          cout << "école militaire +4 et attaque +1 pour les Impérialistes !" << endl;  
        }  
      }	 
      else {  /// colonisation
        game->players[i].getspecial(game->players[i].sectors.size()-1);
      }
    }
  }
  else {
    /// failed sector attack
    cout << "... attaque ratée ! secteur " << game->players[i].missions[m].Bsector << " défendu avec succès par " << game->sectors[Bsectorindex].owner << " (" << -outcome[0] << "/" << -outcome[1] << "/" << -outcome[2] << " flottes restantes, +" << defXgain << "X au défenseur";
    if(defFgain) cout << ", " << defFgain << " flottes capturées";
    cout << ")" << endl; 
    while(defFgain) {   /// captured fleets
      if(Fn(attFF,2)>=defFgain) {   /// preferentially heavy
        defFgain0++;
        defFgain--;        
      }
      else if(Fn(attFF,1)+Fn(attFF,2)>=defFgain) {
        defFgain1++;
        defFgain--;        
      }
      else {
        defFgain0++;
        defFgain--;        
      }
    }    
    game->sectors[Bsectorindex].FF.clear();    
    for(int f=0;f<-outcome[0]+defFgain0;f++) {
      Fleet newfleet;
      newfleet.isl=1;
      game->sectors[Bsectorindex].FF.push_back(newfleet); 
    }
    for(int f=0;f<-outcome[1]+defFgain1;f++) {
      Fleet newfleet;
      newfleet.ism=1;
      game->sectors[Bsectorindex].FF.push_back(newfleet); 
    }
    for(int f=0;f<-outcome[2]+defFgain2;f++) {
      Fleet newfleet;
      newfleet.ish=1;
      game->sectors[Bsectorindex].FF.push_back(newfleet); 
    }      
    if(defpindex>-1) {
      game->players[defpindex].RR[3]+=defXgain;
      if(game->players[defpindex].name=="Nerve") {    /// faction bonus 
        if(game->players[defpindex].sectors[0]->capital==game->players[defpindex].name) {
          game->players[defpindex].sectors[0]->Glevel[0]++;
          game->players[defpindex].sectors[0]->Glevel[2]++;     
        }
        else cout << "ERROR: wrong sector index for capital sector" << endl;   
        cout << "Raffinerie et technosphère +1 pour la Nerve !" << endl;
      }
    }     
  } 
} 

bool Sector::iscentral() {
  if(number==1) return true;
  else return false;
}  
bool Sector::isring() {
  if(number>=2 && number<9) return true;
  else return false;
}  
bool Sector::isbridge() {
  if(number>=9 && number<16) return true;
  else return false;
}  
bool Sector::isperiphery() {
  if(number>=16) return true;
  else return false;
}  

int Sector::victory_points() {
  int points=1;
  if(isring()) points++;
  if(iscentral()) points+=2;
//   if(capital!="no") points+=2;
  if(capital!="no" && owner=="Empire") points++;   /// faction bonus
  for(int k=0;k<megastructures.size();k++) points++;
  points+=extrapoints;
  return points;
}  

void Sector::getspecialupgrades() {
  if(special=="centre_de_recyclage") Glevel[0]+=2;
  if(special=="route_commerciale") Glevel[1]+=2;
  if(special=="université") Glevel[2]+=2;
  if(special=="état-major") Glevel[3]+=2;            
}

float Player::extradef() {
  float extradef=0;
  for(int j=0;j<sectors.size();j++) {
    extradef+=sectors[j]->extradef;
  }  
  if(name=="Nerve") extradef+=nervebonus;  /// faction bonus
  return extradef;
}

float Player::extraatt() {
  float extraatt=0;
  for(int j=0;j<sectors.size();j++) {
    extraatt+=sectors[j]->extraatt;
  }  
  return extraatt;
}

void Player::getspecial(int sectorindex) {
  cout << endl << "**************** spécialité secteur : " << sectors[sectorindex]->special; 
  if(sectors[sectorindex]->special=="garde_royale") {
    cout << "(+20 flottes)" << " "; 
    for(int f=0;f<10;f++) {
      Fleet newfleetm,newfleeth;
      newfleetm.ism=1;
      newfleeth.ish=1;      
      sectors[sectorindex]->FF.push_back(newfleetm);
      sectors[sectorindex]->FF.push_back(newfleeth);
    }  
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="trésor") {
    cout << "(+" << sectorbonusyield << "C)" << " "; 
    RR[1]+=sectorbonusyield;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="bibliothèque") {
    cout << "(+" << sectorbonusyield << "S)" << " ";   
    RR[2]+=sectorbonusyield;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="commandant") {
    cout << "(+" << sectorbonusyield << "X)" << " ";   
    RR[3]+=sectorbonusyield;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="laboratoire") {
    cout << "(technologie gratuite)" << " ";   
    freetech++;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="minotaure") {
    cout << "(+" << sectorbonusattdef << "% défense)" << " ";   
    sectors[sectorindex]->extradef+=sectorbonusattdef;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="hydre") {
    cout << "(+" << sectorbonusattdef << "% attaque)" << " ";     
    sectors[sectorindex]->extraatt+=sectorbonusattdef;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="espion") {
    cout << "(prochaine colonisation gratuite)" << " ";     
    freecolo++;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="général") {
    cout << "(prochaine conquête gratuite)" << " ";     
    freeconq++;
    sectors[sectorindex]->special+="(usé)";
  }
  if(sectors[sectorindex]->special=="gouverneur") {
    cout << "(+1 point de victoire)" << " ";       
    sectors[sectorindex]->extrapoints++;
    sectors[sectorindex]->special+="(usé)";
  }            
  cout << " ********************" << endl << endl;           
}

int Player::victory_points() {
  int points=0;
  for(int j=0;j<sectors.size();j++) {
    points+=sectors[j]->victory_points();
  }  
  return points;
} 

void factioncolours(string faction) {
  if(faction=="Empire") cout << redbold;
  if(faction=="Cardan") cout << magentabold;
  if(faction=="Nerve") cout << greenbold; 
  if(faction=="Synelle") cout << blue;
  if(faction=="Kovahk") cout << bluebold;
  if(faction=="Negore") cout << yellowbold;
  if(faction=="Aphera") cout << cyanbold;
  if(faction=="Magoth") cout << magenta;  
  if(faction=="Imperialistes") cout << yellow;    
  if(faction=="Nouvelle-Nerve") cout << green;      
  if(faction=="Neo-Humaniste") cout << cyan;        
  if(faction=="Ligue") cout << red;          
}  

void Sector::print() {
  if(capital!="no") cout << whitebold;
  cout << "Secteur " << number << reset;
  cout << " : "; 
  factioncolours(owner);
  cout << owner << reset << ", " << Fn(FF,0) << "/" << Fn(FF,1) << "/" << Fn(FF,2) << " flottes;";
  if(printlevel>1) cout << " RR=" << RR << " ";  
  if(printlevel>1 || owner!="rebelle") {
    cout << magenta << " R+" << gain(0) << cyan << " C+" << gain(1) << green << " S+" << gain(2) << red << " X+" << gain(3) << reset;
    cout << " -- " << special;
  }  
  for(int k=0;k<megastructures.size();k++) cout << " -- " << whitebold << megastructures[k]->name << reset;
  cout << " ; " << victory_points() << " pts" << endl;
}  

void Player::print(int nplayers) {
  factioncolours(name);
  cout << name << reset;
  if(isai) cout << " (IA)";
  cout << " : " << RR[1] << cyan << "C" << reset << ", " << RR[2] << green << "S" << reset << ", " << RR[3] << red << "X" << reset << " ; sec ";
  for(int jj=0;jj<sectors.size();jj++) {
    if(sectors[jj]->capital==name) cout << whitebold;
    cout << sectors[jj]->number << reset;
    if(jj<sectors.size()-1) cout << ", ";
  }  
  cout << "; " << "d " << Tlevel[0] << ", a " << Tlevel[1] << ", ad " <<  Tlevel[2] << ", co " << Tlevel[3] << ", in " << Tlevel[4] << ", ch " << Tlevel[5]; 
  cout << " ; V " << whitebold << victory_points() << reset << "/" << victory[nplayers-1] << endl;
}  

void Mission::print(vector<Sector> allsectors) {
  cout << "    ...mission " << number << " : ";
  if(type==0) cout << "renforts ";
  if(type==1) cout << blue << "colonisation " << reset;
  if(type==2) cout << red << "conquête " << reset;
  if(type==3) cout << magenta << "pillage " << reset;
  if(type==4) cout << "patrouille ";
  cout << Fn(FF,0) << "/" << Fn(FF,1) << "/" << Fn(FF,2) << " flottes du secteur " << Asector << " vers le secteur ";
  factioncolours(allsectors[Bsector-1].owner);
  cout << allsectors[Bsector-1].owner << reset << " " << Bsector  << " (" << nturns << " tours, actuellement en secteur " << Csector << ")" << endl;  	
}  

void savegame(int turn,vector<Megastructure> megastructures,vector<Sector> sectors,vector<Player> players,int active_player,string filename) {
  ofstream myfile;
  myfile.open(filename);
  /// asylamba_mini version
  myfile << version << endl;
  /// turn number
  myfile << turn << endl;
  /// active player when the game was saved
  myfile << active_player << endl;
  /// megastructures
  myfile << megastructures.size() << endl;
  for(int me=0;me<megastructures.size();me++) {
    myfile << megastructures[me].number-1 << " ";
    myfile << megastructures[me].inconstruction << " ";
    myfile << megastructures[me].sector << " ";
    myfile << megastructures[me].usedbonus << " " << endl;    
  }  
  /// sectors
  myfile << sectors.size() << endl;
  for(int j=0;j<sectors.size();j++) {
    myfile << sectors[j].number << " ";
    myfile << sectors[j].owner <<  " ";
    myfile << sectors[j].capital <<  " ";
    myfile << sectors[j].FF.size() <<  " ";
    for(int f=0;f<sectors[j].FF.size();f++) {
      myfile << sectors[j].FF[f].isl << " " << sectors[j].FF[f].ism << " " << sectors[j].FF[f].ish << " ";
    }        
    myfile << sectors[j].extradef << " ";
    myfile << sectors[j].extraatt << " ";
    myfile << sectors[j].extrapoints << " ";
    myfile << sectors[j].special << " ";
    for(int kk=0;kk<4;kk++) {
      myfile << sectors[j].Rbonus[kk] <<  " ";
    }  
    myfile << sectors[j].RR <<  " ";
    for(int kk=0;kk<4;kk++) {
      myfile << sectors[j].Glevel[kk] <<  " ";
    }  
    myfile << sectors[j].megastructures.size() << " ";
    for(int me=0;me<sectors[j].megastructures.size();me++) {
      myfile << (sectors[j].megastructures[me]->number)-1 << " ";
    }      
    myfile << sectors[j].Gchain.size() <<  " ";   
    for(int gc=0;gc<sectors[j].Gchain.size();gc++) {
      myfile << sectors[j].Gchain[gc].index << " ";  
      myfile << sectors[j].Gchain[gc].level << " ";  
      myfile << sectors[j].Gchain[gc].turns << " ";  
    }
    myfile << sectors[j].Tchain.size() <<  " ";   
    for(int tc=0;tc<sectors[j].Tchain.size();tc++) {
      myfile << sectors[j].Tchain[tc].index << " ";  
      myfile << sectors[j].Tchain[tc].level << " ";  
      myfile << sectors[j].Tchain[tc].turns << " ";  
    }
    myfile << sectors[j].Fchain.size() <<  " ";   
    for(int fc=0;fc<sectors[j].Fchain.size();fc++) {
      myfile << sectors[j].Fchain[fc].index << " ";  
      myfile << sectors[j].Fchain[fc].level << " ";  
      myfile << sectors[j].Fchain[fc].turns << " ";  
    }    
    myfile << endl; 
  }
  /// players
  myfile << players.size() << endl;
  for(int i=0;i<players.size();i++) {    
    myfile << players[i].name << " ";
    myfile << players[i].isai << " ";
    myfile << players[i].freetech << " ";
    myfile << players[i].freecolo << " ";
    myfile << players[i].freeconq << " ";  
    myfile << players[i].freecoloconq << " ";     
    myfile << players[i].sectors.size() << " ";
    for(int jj=0;jj<players[i].sectors.size();jj++) {
      myfile << (players[i].sectors[jj]->number)-1 << " ";
    }  
    for(int kk=0;kk<4;kk++) {
      myfile << players[i].RR[kk] << " ";
    }  
    for(int kk=0;kk<nTech;kk++) {
      myfile << players[i].Tlevel[kk] << " ";
    }  
    myfile << players[i].missions.size() << " ";
    for(int ll=0;ll<players[i].missions.size();ll++) {
      myfile << players[i].missions[ll].number << " ";
      myfile << players[i].missions[ll].Asector << " ";
      myfile << players[i].missions[ll].Bsector << " ";
      myfile << players[i].missions[ll].Csector << " ";
      myfile << players[i].missions[ll].FF.size() << " ";
      for(int f=0;f<players[i].missions[ll].FF.size();f++) {
        myfile << players[i].missions[ll].FF[f].isl << " " << players[i].missions[ll].FF[f].ism << " " << players[i].missions[ll].FF[f].ish << " ";
      }  
      myfile << players[i].missions[ll].nturns << " ";
      myfile << players[i].missions[ll].type << " ";
      myfile << players[i].missions[ll].outgoing << " ";
    }  
    myfile << endl; 
  }  
  myfile.close();
}  

int Player::AIfleets() {
  int Rtot=sectors[0]->RR;
  int available=0;
  float fractionspare=0;
  if(Tlevel[4]>0) fractionspare=(float) (rand()%80)/100;  /// spare some to perhaps build megastructure later
  int Rspare=round(Rtot*fractionspare);
  available=Rtot-Rspare;  
  return available/fleetcost;
}  

string Player::AIgen() {
  string decision="empty";
  int genlevel[2];
  int gencost[2];
  int R[2];
  int upgrade[2];
  int RCprob[20]={100,100,100,90,60,40,30,10,5,2,1,1,0,0,0,0,0,0,0,0};  /// decision whether to upgrade raffinery or build fleets / mega OR decision whether to upgrade commercial hub or research/colo 
  for(int i=0;i<2;i++) {
    upgrade[i]=0;
    genlevel[i]=sectors[0]->Glevel[i];
    gencost[i]=costupgrade[i]*genlevel[i];
    if(i==0) R[i]=sectors[0]->RR;
    else R[i]=RR[i];
    if(rand()%100<RCprob[genlevel[i]]) upgrade[i]=1;
  }
  /// prioritise raffinery
  if(upgrade[0] && gencost[0]<=R[0]) decision="r";
  /// then commercial hub
  if(upgrade[1] && gencost[1]<=R[1]) decision="p";
  return decision;
}  

string Player::AItech() {
  string decision="empty";
  int teccost[nTech];   /// first index: 0=d, 1=a, 2=ad, 3=co, 4=ar, 5=ch
  for(int l=0;l<nTech;l++) {
    teccost[l]=teccostupgrade[l]*(Tlevel[l]+1);
    if(name=="Aphera") teccost[l]-=apherabonus*teccost[l];
  }
  int probupgrade[nTech];
  int upgrade[nTech];
  for(int l=0;l<nTech;l++) {
    upgrade[l]=0;
    probupgrade[l]=60;
    if(name=="Imperialistes" || name=="Kovahk" || name=="Cardan" || name=="Magoth") {
      if(l==3) probupgrade[l]=100; 
      if(l==4) probupgrade[l]=10; 
    }  
    int ncolo=0;
    for(int m=0;m<missions.size();m++) if(missions[m].type==1 || missions[m].type==2) ncolo++; 
    if(Tlevel[2]>sectors.size()-1+ncolo) probupgrade[l]=probupgrade[l]/2;  /// spare money for colo
    if(Tlevel[l]>2) probupgrade[l]=probupgrade[l]/(Tlevel[l]);
    if(l==2) {    /// admin special
      if(Tlevel[2]>sectors.size()-1+ncolo) probupgrade[l]=0;
      else probupgrade[l]=100;
    }  
    if(rand()%100<probupgrade[l]) upgrade[l]=1+rand()%10;
  }
  /// prioritise administration
  if(upgrade[2] && teccost[2]<=RR[teccurrency[2]] && Tlevel[2]<Tmaxlevel[2]) decision="ad";
  /// then conquest
  else if(upgrade[3] && teccost[3]<=RR[teccurrency[3]] && Tlevel[3]<Tmaxlevel[3]) decision="co";
  /// then frame
  else if(upgrade[5] && teccost[5]<=RR[teccurrency[5]] && Tlevel[5]<Tmaxlevel[5]) decision="ch";
  /// then defense
  else if(upgrade[0] && teccost[0]<=RR[teccurrency[0]] && Tlevel[0]<Tmaxlevel[0]) decision="d";
  /// then attack  
  else if(upgrade[1] && teccost[1]<=RR[teccurrency[1]] && Tlevel[1]<Tmaxlevel[1]) decision="a";
  /// then architecture  
  else if(name!="Imperialistes" && name!="Kovahk" && name!="Magoth" && upgrade[4] && teccost[4]<=RR[teccurrency[4]] && Tlevel[4]<Tmaxlevel[4]) decision="in";
  return decision;
}  

string Player::AIoffers() {
  string decision="empty";
  int Rmax=-1;
  int Rindex=-1;
  int genlevel[4];
  int gencost[4];
  for(int i=0;i<4;i++) {
    genlevel[i]=sectors[0]->Glevel[i];
    gencost[i]=costupgrade[i]*(genlevel[i]+1);
    if(RR[i]>Rmax) {
      Rmax=RR[i];
      Rindex=i;
    }
  }
  if(name=="Negore") {
    if(Rindex==0 && Rmax>3*gencost[Rindex]) decision="r";
    if(Rindex==1 && Rmax>3*gencost[Rindex]) decision="c";
    if(Rindex==2 && Rmax>3*gencost[Rindex]) decision="s";
    if(Rindex==3 && Rmax>3*gencost[Rindex]) decision="x";
  }  
  else {
    if(Rindex==0 && Rmax>5*gencost[Rindex]) decision="r";
    if(Rindex==1 && Rmax>5*gencost[Rindex]) decision="c";
    if(Rindex==2 && Rmax>5*gencost[Rindex]) decision="s";
    if(Rindex==3 && Rmax>5*gencost[Rindex]) decision="x";
  }    
  return decision;
}  

int Player::AIoffershowmuch(int Rindex) {
  int gencost=costupgrade[Rindex]*(sectors[0]->Glevel[Rindex]+1);
  return RR[Rindex]-gencost;
}  

string Player::AIwants() {
  string decision="empty";
  int Rmin=99999999;
  int Rindex=-1;  
  for(int i=1;i<4;i++) {
    if(RR[i]<Rmin) {
      Rmin=RR[i];
      Rindex=i;
    }
  }
//   if(Rindex==0) decision="r";
  if(Rindex==1) decision="c";
  if(Rindex==2) decision="s";
  if(Rindex==3) decision="x";  
  return decision;
}  

int Player::AImissionA() {
  int sectorindex=rand()%sectors.size();
  return sectors[sectorindex]->number;
}  

int Player::AImissionB(vector<Sector> allsectors, int Asector) {
  int decision;
  int nturns;
  bool toofar=1;
  while(toofar) {
    decision=rand()%nsectors+1;
    nturns=hyperdrive(allsectors,Asector,decision)[0];
    int maxnturns=3;
    if(nturns<maxnturns || rand()%50==1) toofar=0;
  }  
  return decision;
}  

float Player::AImissionhowmuch(vector<Sector> allsectors, int Asector, int Bsector) {
  int nfleets=0;
  float fractionsend;
  if(name=="Kovahk" || name=="Cardan" || name=="Magoth") fractionsend=0.9;
  else fractionsend=0.7;
  if(allsectors[Asector-1].capital!="no") {   //// leave more defense in capital
    fractionsend-=0.3+rand()%30/100;
  }
  else {
    fractionsend-=rand()%30/100;
  }  
  if(fractionsend*allsectors[Asector-1].FF.size()>2 && fractionsend*allsectors[Asector-1].FF.size()>allsectors[Bsector-1].FF.size()+1) { 
    nfleets=fractionsend*allsectors[Asector-1].FF.size();
  }  
  return nfleets;  
}

string Player::AImissiontype(int cancolo, int canconq, int isrebel) {
  string decision="c";    /// colonise or conquer by default
  return decision;
}  

///////////////////////////////////////////////

int randgain(int mean) {
  int half=(int) floor(mean/2);
  int gain=half+rand()%(2*half);
  return gain;
}  

void scores(vector<Player> players) {

}  



